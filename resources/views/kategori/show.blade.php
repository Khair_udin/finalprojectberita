@extends('layouts.master')

@section('judul')
Halaman Detail Kategori {{$kategori->nama}}
@endsection

@section('content')
    <h4>Kategori: {{$kategori->nama}}</h4>
    <p>{{$kategori->deskripsi}}</p>

    <a href="/kategori" class="btn btn-secondary">Kembali</a>
@endsection