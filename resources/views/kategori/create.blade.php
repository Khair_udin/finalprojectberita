@extends('layouts.master')

@section('judul')
Halaman Tambah Kategori
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/bmz1cy53yl0hmtawztot8mn66z9587qade0qc4qpcvicck6m/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush
@section('content')
<form action="/kategori" method="POST">
@csrf
  <div class="form-group">
    <label>Nama Kategori</label>
    <input type="text" name="nama" class="form-control">
  </div>
  @error('nama')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Deskripsi</label>
    <textarea name="deskripsi" class="form-control"></textarea>
  </div>
  @error('deskripsi')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection