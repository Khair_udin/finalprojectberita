<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.master');
});

Route::group(['middleware' => ['auth']], function(){
    // CRUD Kategori
    Route::resource('kategori', 'KategoriController');

    //Update Profile
    Route::resource('profil', 'ProfileController')->only([
        'index', 'update'
    ]);

    Route::resource('komentar', 'KomentarController')->only([
        'index', 'store'
    ]);


});



// CRUD Berita
Route::resource('berita', 'BeritaController');



Auth::routes();
